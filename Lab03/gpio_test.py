#!/usr/bin/python

###############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 3: LED Web Server
# Leo Salemann  leos@uw.edu  leo.salemann@me.com
# 10/26/17
###############################################################

import time
from gpio import PiGpio

# create an instance of the pi gpio driver.
pi_gpio= PiGpio()

# Blink the LEDS forever.
print('Blinking all my LEDs in sequence (Ctrl-C to stop)...')
sleep_sec = 5.0

while True:
# Get the current switch state and print

    switch = pi_gpio.read_switch()
    print('\n============ Switch: {0} ============'.format(switch))

    print('\nLED 1 ON (RED)')
    pi_gpio.set_led(1,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(sleep_sec)

    print('\nLED 2 ON (GRN)')
    pi_gpio.set_led(2,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(sleep_sec)

    print('\nLED 3 ON (BLU)')
    pi_gpio.set_led(3,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(sleep_sec)


    print('\nLED 1 OFF (RED)')
    pi_gpio.set_led(1,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(sleep_sec)

    print('\nLED 2 OFF (GRN)')
    pi_gpio.set_led(2,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(sleep_sec)

    print('\nLED 3 OFF (BLU)')
    pi_gpio.set_led(3,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(sleep_sec)
