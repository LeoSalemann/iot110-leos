#!/usr/bin/python
 
# altimiter_test.py
##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 7: PWM Control
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 12/17/17
##############################################################

import time
from altimeter import PiAltimeter

print "begin altimiter test"
pi_alt = PiAltimeter()

print "  reset the altimieter"
# Zero out the Stepper moter
pi_alt.reset()

print "  get altitutde: ",  pi_alt.getAltitude()


print " goto 5000 feet"
pi_alt.setAtltitude(5000)

time.sleep(10)

print "goto 2000 feet"
pi_alt.setAtltitude(2000)
time.sleep(10)

print "goto 1233.45 feet"
pi_alt.setAtltitude(1233.45)
time.sleep(10)


###################
print "  climb"
###################
for altitude in range (0, 10000, 100):
    print "  set altitude to: ", altitude
    pi_alt.setAtltitude(altitude)
    time.sleep(0.5)
    print "  altimiter says: ", pi_alt.getAltitude()

###################
print "  descend"
###################
for altitude in range (9900, 5000, -100):
    print "  set altitude to: ", altitude
    pi_alt.setAtltitude(altitude)
    time.sleep(0.5)
    print "  altimiter says: ", pi_alt.getAltitude()

###################
print "  cruise"
###################
for altitude in range (9900, 5000, -100):
    print "  set altitude to: ", 5000
    pi_alt.setAtltitude(5000)
    time.sleep(0.5)
    print "  altimiter says: ", pi_alt.getAltitude()


print "altimiter test complete."