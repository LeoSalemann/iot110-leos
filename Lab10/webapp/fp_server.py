#!/usr/bin/python

##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 10: Flight Panel Server
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 12/21/17
# TO-DO: add support for the full JSON
##############################################################

import time
import datetime
import pprint

# The line below shoudl probbably say PiFlightPanel, 
# which would have a PiFlightPanel.ASI
from flight_panel import PiAirSpeed 

from flask import *
app = Flask(__name__)

# Create flight instruments, based on stepper motors
# All of these classes reference stepper.py
pi_asi = PiAirSpeed()  # Air Speed Indicator
# pi_vsi = PiClimbRate() # Vertical Air Speed (e.g. Climb rate) indicator


@app.route("/")
def index():
    return render_template('index.html')
# ============================== API Routes ===================================


# # =================== GET: /set_altitude/<float:altitude> =====================
# # set the motor steps (int) by HTTP GET method  CURL example:
# # curl http://<pi host>:5000/set_altitude/1000
# # -----------------------------------------------------------------------------
# @app.route("/set_altitude/<float:altitude>", methods=['GET'])
# def set_altitude(altitude):
#     pi_alt.setAtltitude(altitude)
#     time.sleep(0.1)
#     return "Altitude : " + str(pi_alt.getAltitude()) + "\n"


# ==================== GET: /set_airspeed/<float:airspeed> ====================
# set the motor steps (int) by HTTP GET method  CURL example:
# curl http://<pi host>:5000/set_airspeed/100
# -----------------------------------------------------------------------------
@app.route("/set_airspeed/<float:airspeed>", methods=['GET'])
def set_airspeed(airspeed):
    pi_asi.setAirSpeed(airspeed)
    time.sleep(0.1)
    return "Knots Indicated Air Speed (KIAS) : " + str(pi_asi.getAirSpeed()) + "\n"


# curl --data 'mykey=FOOBAR' http://0.0.0.0:5000/createHello
# echo 'mykey={"name":"Carrie Fisher","age":"60"}' | curl -d @- http://0.0.0.0:5000/createHello
@app.route('/test', methods = ['POST'])
def postRequestTest():
    mydata = request.data

    # import pdb; pdb.set_trace()
    return "Hello API Server : You sent a "+ request.method + \
            " message on route path " + request.path + \
            " \n\tData:" +  data + "\n"


# ============================ END API Routes =================================

# ============================= Run App Server ================================
# =========================== Endpoint: /myData ===============================
# read the sensor values by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_values():
        while True:
            # return the yield results on each loop, but never exits while loop

            # Could add the whole simvar json right here
            data_obj = {'meas_time': datetime.datetime.now().isoformat()
            }

            yield('data: {0}\n\n'.format(json.dumps(data_obj)))

            time.sleep(0.2) #How small can I make this? 0.2 sec works.

    return Response(get_values(), mimetype='text/event-stream')
# ============================== API Routes ===================================

if __name__ == "__main__":

    pi_asi.reset()

    app.run(host='0.0.0.0', debug=True, threaded=True)
# =============================================================================
