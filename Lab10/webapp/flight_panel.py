#!/usr/bin/python

# flight_panel.py
##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 10: Flight Panel
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 12/21/17
##############################################################

import time
from stepper import PiStepper

Ascending = 1
Descending  = 0

CW = 1
CCW  = 0

# 100 steps is 200 feet  REAL ALTIMITER
ALTITUDE_TO_STEPS = 0.25
STEPS_TO_ALTITUDE = 2

# 300 steps is 20 knots (AIRSPEED FAKEOUT)
AIRSPEED_TO_STEPS = 15        # 300/20
STEPS_TO_AIRSPEED = 0.06667   # 20/300


# ================< SETUP INSTRUMENTS >================
# *** THIS WILL ONLY WORK ONCE PiStepper CAN SET   ***
# *** GPIO PINS SEPARATELY FOR EACH ISNTRUMENT !!! ***                  

# pi_smc_alt = PiStepper() # Altimiter
pi_smc_asi = PiStepper() # Air Speed Idicator

# ================< SETUP INSTRUMENTS >================


# This will probably evolve to be PiFlightPanel
# Can I have more than one class in a python file?
class PiAirSpeed(object):
    def __init_(self, altitude=0):
        self.altitude = 0
        self.airspeed = 0


    # TODO: Need to be able to tell pi_smc what GPIO pins to use
    # # ============< ALTIMITER > ===========================

    # # -----------------------------------------------------
    # # reset_altimeter()
    # # 
    # # Sweep the needel its full extent, move it to the
    # # zero position, set for clockwise operation.
    # # -----------------------------------------------------
    # def reset_altimiter(self):
    #     pi_smc_alt.start()
    #     pi_smc_alt.setSpeed(100)
    #     print 'Finding Home Position...'
    #     pi_smc_alt.setDirection(CCW)
    #     pi_smc_alt.step(3200)
    #     pi_smc_alt.setPosition(0)
    #     pi_smc_alt.setDirection(CW)
        
    # # -----------------------------------------------------
    # # set_altitude()
    # # 
    # # Move the needle to the target altitude
    # # Convert alitude to motor steps, set the stepper motor
    # # -----------------------------------------------------
    # def setAtltitude(self, altitude):
    #     # convert altitude to pwm steps
    #     pi_smc_alt.goto(int(altitude * ALTITUDE_TO_STEPS))
    #     self.altitude = altitude

    # # -----------------------------------------------------
    # # get_altitude()
    # # 
    # # Query the stepper motor
    # # Convert moter steps to altitude & return
    # # -----------------------------------------------------
    # def getAltitude(self):
    #     current_steps = pi_smc_alt.getPosition()
    #     self.altitude = current_steps * STEPS_TO_ALTITUDE
    #     return current_steps

    # # ============< ALTIMITER > ===========================


    # ============< AIRSPEED > ===========================

    # -----------------------------------------------------
    # reset_airspeed()
    # 
    # Sweep the needel its full extent, move it to the
    # zero position, set for clockwise operation.
    # -----------------------------------------------------
    def reset_airspeed(self):
        pi_smc_asi.start()
        pi_smc_asi.setSpeed(100)
        print 'Finding Home Position...'
        pi_smc_asi.setDirection(CCW)
        pi_smc_asi.step(3200)
        pi_smc_asi.setPosition(0)
        pi_smc_asi.setDirection(CW)

        
    # -----------------------------------------------------
    # set_airspeed()
    # 
    # Move the needle to the target altitude
    # Convert alitude to motor steps, set the stepper motor
    # -----------------------------------------------------
    def setAirSpeed(self, airspeed):
        # convert altitude to pwm steps
        pi_smc_asi.goto(int(airspeed * AIRSPEED_TO_STEPS))
        self.airspeed = airspeed

    # -----------------------------------------------------
    # get_airspeed()
    # 
    # Query the stepper motor
    # Convert moter steps to altitude & return
    # -----------------------------------------------------
    def getAirSpeed(self):
        current_steps = pi_smc_asi.getPosition()
        self.airspeed = current_steps * STEPS_TO_AIRSPEED
        return current_steps

    # ============< AIRSPEED > ===========================


    # ============< PANEL > ===========================

    # -------------------------------------------------
    # get_altitude()
    # 
    # reset all instruments in the panel
    def reset(self):
        # self.reset_altimiter()
        self.reset_airspeed()
    # ============< PANEL > ===========================