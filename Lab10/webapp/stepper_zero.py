
#!/usr/bin/python
import time
from stepper import PiStepper

CW = 1
CCW  = 0

# Create a stepper motor controller object
pi_smc = PiStepper()
print('Motor Controller Initialized')

# set the initial RPM speed

pi_smc.start()
pi_smc.setSpeed(100)
print 'Finding Home Position...'
pi_smc.setDirection(CCW)
pi_smc.step(3200)
pi_smc.setPosition(0)
print('Stopping Motor')
pi_smc.stop()
