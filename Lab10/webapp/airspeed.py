#!/usr/bin/python

# altimiter.py
##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 10: Altimeter
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 12/21/17
##############################################################

import time
from stepper import PiStepper

Ascending = 1
Descending  = 0

CW = 1
CCW  = 0

# 100 steps is 200 feet  REAL ALTIMITER
ALTITUDE_TO_STEPS = 0.25
STEPS_TO_ALTITUDE = 2

pi_smc = PiStepper()

# print('Motor Controller Initialized')

# ----------------------------------------
# reset()
#
# Move needle to the zero position, ready to move clockwise.
# ----------------------------------------
class PiAltimeter(object):
    def __init_(self, altitude=0):
        self.altitude = 0

    def reset(self):
        pi_smc.start()
        pi_smc.setSpeed(100)
        print 'Finding Home Position...'
        pi_smc.setDirection(CCW)
        pi_smc.step(3200)
        pi_smc.setPosition(0)
        pi_smc.setDirection(CW)
        

    # ----------------------------------------
    # set_altitude()
    # 
    # Move the needle to the target altitude
    # ----------------------------------------
    def setAtltitude(self, altitude):
        # convert altitude to pwm steps
        pi_smc.goto(int(altitude * ALTITUDE_TO_STEPS))
        self.altitude = altitude


    # get_altitude()
    def getAltitude(self):
        current_steps = pi_smc.getPosition()
        self.altitude = current_steps * STEPS_TO_ALTITUDE
        return current_steps