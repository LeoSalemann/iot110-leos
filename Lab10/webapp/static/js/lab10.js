/*
##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 7: PWM Control
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 12/17/17
##############################################################
*/

$(document).ready(function() {

    var max_data_saved = 4;
    //Add variable to hold data between receive data events
    var sse_sensor_data = [];
    //var parsed_json_data = [];
    // the key event receiver function
    iotSource.onmessage = function(e) {
        getFSX();
    }

    // ----< from https://www.html5rocks.com/en/tutorials/cors/ >---
    // ============================ Create CORS Request ========================
    function createCORSRequest(method, url) {
        console.log('...createCORSRequest(' + method +', ' + url + ')');
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
          console.log('...with credentials');
          // Check if the XMLHttpRequest object has a "withCredentials" property.
          // "withCredentials" only exists on XMLHTTPRequest2 objects.
          xhr.open(method, url, true);
        
        } else if (typeof XDomainRequest != "undefined") {
            console.log('...no credentials');
          // Otherwise, check if XDomainRequest.
          // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
          xhr = new XDomainRequest();
          xhr.open(method, url);
      
        } else {
            console.log('...returning null');
            // Otherwise, CORS is not supported by the browser.
          xhr = null;
      
        }
        console.log('...done');
        return xhr;
      }
    // ============================ Create CORS Request ========================


    // ============================ FSX REST API ===============================
    function getFSX(){
        console.log("calling REST API ... ")

        var url = "http://10.211.55.6:8000/getall"
        var xhr = createCORSRequest('GET', url);
        if (!xhr) {
             console.log('CORS not supported');
        }

        xhr.setRequestHeader("Content-type", "application/json");

        // Response handlers.
        xhr.onload = function() {
            var jtxt = JSON.parse(xhr.responseText);
            var airspeed = jtxt['Airspeed_Indicated'];
            
            console.log(jtxt);
            console.log(airspeed);
            $.get('/set_airspeed/' + airspeed);
            $('#motor_position').text(jtxt['Airspeed_Indicated']);
        };

        xhr.send();
        console.log('done');
    }
    // ============================ FSX REST API ===============================

    // Renders the jQuery-ui elements
    $("#tabs").tabs();

});