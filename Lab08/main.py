###############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 5: LED Web Server
# Leo Salemann  leos@uw.edu  leo.salemann@me.com
# 11/09/17
###############################################################

import time
import datetime
from sense import PiSenseHat
import paho.mqtt.client as paho
from flask import *

app = Flask(__name__)

# create Pi SenseHat Object
pi_sense_hat = PiSenseHat()
pi_sense_hat.setPixels()

# ============================== Functions ====================================
def get_sensor_values():
    return pi_sense_hat.getAllSensors()

# ============================= MQTT Callbacks ================================
# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))

# The callback for when a PUBLISH message is received from the broker.
def on_message(client, userdata, msg):
    print (string.split(msg.payload))
# ============================= MQTT Callbacks ================================

# MQTT Configuration for local network
localBroker = "localhost"   # Local MQTT broker
localPort   = 1883          # Local MQTT port
localUser   = "pi"          # Local MQTT user
localPass = "stoopid"       # Local MQTT password
localTopic = "iot/sensor"   # Local MQTT topic to monitor
localTimeOut = 120          # Local MQTT session timeout

# Setup to Publish Sensor Data
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect(localBroker, localPort, localTimeOut)





# ============================== API Routes ===================================

@app.route("/")
def index():
    return render_template('index.html')


# =========================== Endpoint: /myData ===============================
# read the gpio states by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            data_payload = get_sensor_values()
            yield('data: {0}\n\n'.format(data_payload))
            print("MQTT Topic:"+localTopic, data_payload)
            mqttc.publish(localTopic,data_payload)
            time.sleep(2.0)
    return Response(get_values(), mimetype='text/event-stream')


# ============================ GET: /leds/<state> =============================
# read the LED status by GET method from curl for example
# curl http://iot8e3c:5000/led/1
# curl http://iot8e3c:5000/led/2
# -----------------------------------------------------------------------------
@app.route("/leds/<int:led_state>", methods=['GET'])
def leds(led_state):
  return "LED State:" + str(pi_gpio.get_led(led_state)) + "\n"


# =============================== GET: /sw ====================================
# read the switch input by GET method from curl for example
# curl http://iot8e3c:5000/sw
# -----------------------------------------------------------------------------
@app.route("/sw", methods=['GET'])
def sw():
  return "Switch State:" + str(pi_gpio.read_switch()) + "!\n"

# @app.route('/ledtoggle', methods=['POST'])
#	led = int(str(request.form['led']))
#	pi_gpio.set_led(led, not pi_gpio.get_led(led))

# ======================= POST: /ledcmd/<data> =========================
# set the LED state by POST method from curl. For example:
# curl --data 'led=1&state=ON' http://iot8e3c:5000/ledcmd
# -----------------------------------------------------------------------------
@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    cmd_data = request.data
    print "LED Command:" + cmd_data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pi_gpio.set_led(led,False)
    elif (state == 'ON'):
        pi_gpio.set_led(led,True)
    else:
        return "Argument Error"

    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"
    # -----------------------------------------------------------------------------




# ============================== API Routes ===================================


###############< NO MORE APIS BELOW THIS >######################

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)