###############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 3: LED Web Server
# Leo Salemann  leos@uw.edu  leo.salemann@me.com
# 10/26/17
###############################################################

SHIFT_MASK = 15 #1111 in binary

class Debouncer(object):
    """Shift Register Debouncer"""

    def __init__(self):
        self.switch_shift_register = 0

    # perform AND logic debouncer
    def debounce(self, switch_in):
        self.switch_shift_register = (self.switch_shift_register << 1) | switch_in
        return int((self.switch_shift_register & SHIFT_MASK) == SHIFT_MASK)