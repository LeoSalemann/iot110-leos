/*
##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT110
# Lab 7: PWM Control
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 12/17/17
##############################################################
*/

$(document).ready(function() {

    var max_data_saved = 4;
    //Add variable to hold data between receive data events
    var sse_sensor_data = [];
    //var parsed_json_data = [];
    // the key event receiver function
    iotSource.onmessage = function(e) {
        // parse sse received data
        // console.log(e.data);
        var parsed_json_data = JSON.parse(e.data);

        // For now just log to console
        console.log(parsed_json_data);

        // Convert string datetime to JS Date object
        parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);
        // console.log(parsed_json_data);

        // Push new data to front of saved data list
        sse_sensor_data.unshift(parsed_json_data);
        // Then remove oldest data up to maximum data saved
        while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

        updateEnvironmentalTable();
        //updateSwitchIndicator();

        updateStepperMotor(parsed_json_data);
    }

    // Buttons
    $('#motor_start').click(function() {
        console.log('Start Motor Up!');
/*
        $.get('/motor_speed/' + $('#motor_speed').val());
        $.get('/motor_direction/'+$('#motor_direction').val());
     //   $.get('/motor_position/'+$('#motor_position').val());
        $.get('/motor_steps/'+$('#motor_steps').val());
        */
        $.get('/motor/1');
    });

    $('#motor_stop').click(function() {
        console.log('Stop Motor');
        $.get('/motor/0');
    });
    $('#motor_zero').click(function() {
        console.log('Zero Motor Position');
        $.get('/motor_zero');
    });
    

    $('#motor_multistep').click(function() {
        var params = 'steps='+$('#motor_steps').val()+"&direction="+$('#motor_direction').val();
        console.log('Multistep with params:' + params);
        $.post('/motor_multistep', params, function(data, status){
                    console.log("Data: " + data + "\nStatus: " + status);
                });
    });
    

    // Text Fields
    $('#motor_speed').change(function() {
        console.log('Changed motor speed to ' + $('#motor_speed').val());
        $.get('/motor_speed/' + $('#motor_speed').val());
    });

    $('#motor_position').change(function() {
        console.log('Changed motor position to ' + $('#motor_position').val());
        $.get('/motor_position/'+$('#motor_position').val());
    });

    $('#motor_steps').change(function() {
        console.log('Changed motor steps to ' + $('#motor_steps').val());
        $.get('/motor_steps/'+$('#motor_steps').val());
    });

    $('#motor_direction').change(function() {
        console.log('Changed motor steps to ' + $('#motor_direction').val());
        $.get('/motor_direction/'+$('#motor_direction').val());
    });

    // ============================ STEPPER MOTOR ===============================
    function updateStepperMotor(data) {
        $('#motor_position').text(data['motor']['position']);
/*
        $.get('/motor_speed/' + $('#motor_speed').val());
        // $.get('/motor_position/'+$('#motor_position').val());
        $.get('/motor_steps/'+$('#motor_steps').val());
        $.get('/motor_direction/'+$('#motor_direction').val());
*/
        if (data['motor']['state'] === '1') {
          $('#motor_state').toggleClass('label-default', false);
          $('#motor_state').toggleClass('label-success', true);
        } else if (data['motor']['state'] === '0') {
          $('#motor_state').toggleClass('label-default', true);
          $('#motor_state').toggleClass('label-success', false);
        }    
    }
    // ============================ STEPPER MOTOR ===============================

    // ============================== ENV TABLE =================================

    function updateEnvironmentalTable() {
        // console.log(sse_sensor_data)
        // ... add code here ...
        rStr = "";
        sse_sensor_data.slice(0, 5).forEach(function(di){
            /*
            console.log(di);
            console.log(di['meas_time']);
            console.log(di['environmental']['temperature']['value']);
            console.log(di['environmental']['pressure']['value']);
            */
            rStr += "<tr>";
            rStr += "<td>" + di['meas_time'] + "</td>";
            rStr += "<td>" + di['environmental']['temperature']['value'].toFixed(2) + "</td>";
            rStr += "<td>" + di['environmental']['pressure']['value'].toFixed(2) + "</td>";
            rStr += "</tr>";        
        });

        $("tbody#sensor-data").html(rStr);
    }
    // ============================== / ENV TABLE ===============================

    // Renders the jQuery-ui elements
    $("#tabs").tabs();

    // ===================================================================
    // LED 1 SLIDER
    $("#slider1").slider({
        orientation: "vertical",
        min: 0,
        max: 100,
        value: 50,
        animate: true,
        slide: function(event, ui) {
            var dcValue = ui.value;
            $("#pwm1").val(dcValue);
            console.log("green led duty cycle(%):", ui.value);
            $.post('/set_pwm', { channel: 1, dutyCycle: ui.value });
        }
    });
    $("#pwm1").val($("#slider1").slider("value"));

    // ===================================================================
    // LED 2 SLIDER
    $("#slider2").slider({
        orientation: "vertical",
        min: 0,
        max: 100,
        value: 50,
        animate: true,
        slide: function(event, ui) {
            var dcValue = ui.value;
            $("#pwm2").val(dcValue);
            console.log("2 led duty cycle(%):", ui.value);
            $.post('/set_pwm', { channel: 2, dutyCycle: ui.value });
        }
    });
    $("#pwm2").val($("#slider2").slider("value"));
    // ===================================================================

    // ===================================================================
    // LED 3 SLIDER
    $("#slider3").slider({
        orientation: "vertical",
        min: 0,
        max: 100,
        value: 50,
        animate: true,
        slide: function(event, ui) {
            var dcValue = ui.value;
            $("#pwm3").val(dcValue);
            console.log("3 led duty cycle(%):", ui.value);
            $.post('/set_pwm', { channel: 3, dutyCycle: ui.value });
        }
    });
    $("#pwm3").val($("#slider3").slider("value"));
    // ===================================================================
});