$(document).ready(function() {

  // Add variable to set maximum number of data point saved and table length
  var max_data_saved = 20;
  //Add variable to hold data between receive data events
  var sse_sensor_data = [];



  // from https://www.taniarascia.com/how-to-connect-to-an-api-with-javascript/
  // Create a request variable and assign a new XMLHttpRequest object to it.
  //var request = new XMLHttpRequest();





  // the key event receiver function
  iotSource.onmessage = function(e) {
    // parse sse received data
    parsed_json_data = JSON.parse(e.data);
    console.log(parsed_json_data);
    parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);

    // Push new data to front of saved data list
    sse_sensor_data.unshift(parsed_json_data);

    console.log("try post to hashstax");
    /*
    request.open('POST', 'http://10.177.0.3:9191/storage' 
    -H 'accept: application/json' 
    -H 'Originator-Ref: hello' 
    -H 'Content-Type: application/json' 
    -d e.data);
    */
    // from https://stackoverflow.com/questions/49117235/how-can-i-send-a-curl-request-from-javascript
        $.ajax({
            url: "http://10.177.0.3:9191/storage",
            type: 'POST',
            dataType: 'json',
            headers: {
                'accept': 'application/json',
                'Originator-Ref': 'hello',
                'contentType': 'application/json',
            },
            // data: {"from":"pi"},
            data: parsed_json_data,
            //data: JSON.stringify(e.data),
            success: function (result) {
              alert(result);
            },
            error: function (error) {
               alert("Cannot get data");
            }
        });

    console.log("posted");

    // Then remove oldest data up to maximum data saved
    while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

    // Call the function that updates the environmental sensors table
    updateEnvironmentalTable();
    update_env_chart();

    updateInertialTable();
    update_imu_chart();
    update_ori_chart();

    updateJoystickTable();

  }

  // ============================== ENV TABLE ================================
  function updateEnvironmentalTable() {
    // Iterate over each environmental parameter row
    $('tr.env-param-row').each(function(i) {
        // Start new string of output html, add in measurement time
        var new_html = '<td>' + sse_sensor_data[i]['meas_time'].toLocaleString() + '</td>';
        // And add in each specific env sensor data value
        new_html += '<td>' + sse_sensor_data[i]['environmental']['temperature'].value.toFixed(2) + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['environmental']['pressure'].value.toFixed(2) + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['environmental']['humidity'].value.toFixed(2) + '</td>';

        // Then replace the current html with this newly created table row
        $(this).html(new_html);
      })
  }
  // ============================== ENV TABLE ================================


  // In SSE OnMessage Event Handler add function call to update_env_chart()

  // ============================== ENV CHART ================================
  // initialize the accel chart structure
  var env_chart = new Morris.Line({
      element: 'env-chart',
      data: '',
      xkey: 'time',
      ykeys:  ['humidity', 'temp'],
      labels: ['%RH',     '&degC']
  });

  // build the environmental chart data array for MorrisJS structure
  function update_env_chart(data) {
      var chart_data = [];
      // Create chart data object to pass to morris chart object
      sse_sensor_data.forEach(function(d) {
          env_record = {
              'time': d['meas_time'].getTime(),
              'humidity': d['environmental']['humidity'].value,
              'temp': d['environmental']['temperature'].value
          };
          chart_data.push(env_record);
      });
      env_chart.setData(chart_data);
  };
  // ============================== ENV CHART ================================

  // ============================== IMU TABLE ================================
  function updateInertialTable() {
    // Iterate over each environmental parameter row
    $('tr.imu-param-row').each(function(i) {
        // Start new string of output html, add in measurement time
        var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
        // And add in each specific env sensor data value
        new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer'].x.toFixed(2) + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer'].y.toFixed(2) + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer'].z.toFixed(2) + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation'].pitch.toFixed(2) + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation'].roll.toFixed(2) + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation'].yaw.toFixed(2) + '</td>';      
        new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation'].compass.toFixed(2) + '</td>';


        // Then replace the current html with this newly created table row
        $(this).html(new_html);
      })
  }
  // ============================== IMU TABLE ================================


  // ============================== IMU CHART ================================
  // initialize the accel chart structure
  var imu_chart = new Morris.Line({
      element: 'imu-chart',
      data: '',
      xkey: 'time',
      ykeys:  ['x',       'y',       'z'],
      labels: ['Accel-X', 'Accel-Y', 'Accel-Z']
  });

  // build the environmental chart data array for MorrisJS structure
  function update_imu_chart(data) {
      var imu_chart_data = [];
      // Create chart data object to pass to morris chart object
      sse_sensor_data.forEach(function(d) {
          imu_record = {
              'time': d['meas_time'].getTime(),
              'x': d['inertial']['accelerometer'].x,
              'y': d['inertial']['accelerometer'].y,
              'z': d['inertial']['accelerometer'].z 
          };
          imu_chart_data.push(imu_record);
      });
      imu_chart.setData(imu_chart_data);
  };
  // ============================== IMU CHART ================================

  // ============================== ORIENTATION CHART ================================
  // initialize the accel chart structure
  var ori_chart = new Morris.Line({
    element: 'ori-chart',
    data: '',
    xkey: 'time',
    ykeys:  ['p',     'r',    'y'],
    labels: ['Pitch', 'Roll', 'Yaw']
});

  // build the environmental chart data array for MorrisJS structure
  function update_ori_chart(data) {
    var ori_chart_data = [];
    // Create chart data object to pass to morris chart object
    sse_sensor_data.forEach(function(d) {
        ori_record = {
            'time': d['meas_time'].getTime(),
            'p': d['inertial']['orientation'].pitch,
            'r': d['inertial']['orientation'].roll,
            'y': d['inertial']['orientation'].yaw 
        };
        ori_chart_data.push(ori_record);
    });
    ori_chart.setData(ori_chart_data);
};
// ============================== ORIENTATION CHART ================================


  // ============================== JOYSTICK TABLE ================================
  function updateJoystickTable() {
    // Iterate over each environmental parameter row
    $('tr.joy-param-row').each(function(i) {
        // Start new string of output html, add in measurement time
        var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
        // And add in each specific env sensor data value
        new_html += '<td>' + sse_sensor_data[i]['joystick'].action + '</td>';
        new_html += '<td>' + sse_sensor_data[i]['joystick'].direction + '</td>';

        // Then replace the current html with this newly created table row
        $(this).html(new_html);
      })
  }
  // ============================== JOYSTICK TABLE ================================

})
