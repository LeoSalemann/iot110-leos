#!/usr/bin/python

# =============================================================================
#        File : joystick_test.py
# Description : Test harness for SenseHat joystick
#      Author : L. Salemann
#        Date : 11/28/17
# =============================================================================
#
#  Official sense-hat API available from :
#  http://pythonhosted.org/sense-hat/api/
#
# =============================================================================
 
''' from sense_hat import SenseHat
sense = SenseHat()
while True:
    print("looking for events")
    for event in sense.stick.get_events():
        print("The joystick was {} {}".format(event.action, event.direction)) '''

''' from sense_hat import SenseHat
sense = SenseHat()
while True:
#    print("looking for events")
    for event in sense.stick.get_events():
        print("     found an event")
        print(event.direction, event.action)
#   print("  done with for loop") '''



''' from sense_hat import SenseHat
from time import sleep

sense = SenseHat()
event = sense.stick.wait_for_event()
print("The joystick was {} {}".format(event.action, event.direction))
sleep(0.1)
event = sense.stick.wait_for_event()
print("The joystick was {} {}".format(event.action, event.direction))
 '''

from sense_hat import SenseHat

sense = SenseHat()
while True:
    for event in sense.stick.get_events():
        print("The joystick was {} {}".format(event.action, event.direction))