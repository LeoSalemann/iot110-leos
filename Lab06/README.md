UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION

Internet of Things Certificate Program IOT110

Lab 6: SenseHAT

Leo Salemann  leos@uw.edu  leo.salemann@me.com

11/30/2017


# EnvironTable
![EnvironTable](pics/EnviroTable.PNG)

# EnvironChart
![EnvironTable](pics/EnviroChart.PNG)

# IntertialTable
![IMUtable](pics/IMU_Table.PNG)

# InertialChart
![IMUchart](pics/IMU_Chart.PNG)

# OrientationChart

Added this one myself
![ORIchart](pics/OrientationChart.PNG)

Picture captured with tilted RPI (orientation doesn't change much, for some reason):
![ORIpic](pics/TiltedPI.JPG)

# JoyStick

Got some code in place, not picking up stick pushes, though.

![JOYtable](pics/JoystickTable.PNG)


# Display

Backend python turns on LED's, no front-end support.